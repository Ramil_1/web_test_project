import React from 'react';
import ReactDom from 'react-dom';

const App = () => <h1>Hello world для проекта №1 - web_test_project</h1>

export default () => <App/>;

export const mount = (Сomponent) => {
    ReactDom.render(
        <Сomponent/>,
        document.getElementById('app')
    );
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};

